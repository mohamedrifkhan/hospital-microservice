package com.ont.hospitalmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class HospitalMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HospitalMicroserviceApplication.class, args);
	}

}
