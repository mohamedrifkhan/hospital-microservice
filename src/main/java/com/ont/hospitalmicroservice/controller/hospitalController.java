package com.ont.hospitalmicroservice.controller;

import com.ont.hospitalmicroservice.model.hospital;
import com.ont.hospitalmicroservice.service.hospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/hospital/*")
public class hospitalController {
    @Autowired
    private hospitalService hospitalService;

    @GetMapping(value = "get")
    String get(){
        return "Hello Hospital";
    }

    @GetMapping(value = "getAll")
    List<hospital> getAll(){
        return hospitalService.getAll();
    }
}
