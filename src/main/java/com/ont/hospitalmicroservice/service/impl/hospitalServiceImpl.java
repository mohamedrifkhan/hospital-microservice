package com.ont.hospitalmicroservice.service.impl;

import com.ont.hospitalmicroservice.model.hospital;
import com.ont.hospitalmicroservice.repo.hospitalRepo;
import com.ont.hospitalmicroservice.service.hospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class hospitalServiceImpl implements hospitalService {
    @Autowired
    private hospitalRepo hospitalRepo;

    @Override
    public List<hospital> getAll() {
        List<hospital> hospitalList=hospitalRepo.findAll();
        return hospitalList;
    }
}
