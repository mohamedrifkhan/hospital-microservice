package com.ont.hospitalmicroservice.repo;

import com.ont.hospitalmicroservice.model.hospital;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface hospitalRepo extends JpaRepository<hospital,Long> {
}
