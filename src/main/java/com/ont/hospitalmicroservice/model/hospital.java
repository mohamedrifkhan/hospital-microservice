package com.ont.hospitalmicroservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hospital")
public class hospital {
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "cod_hospital")
    private long hospitalCode;
    @Column(name = "nombre")
    private String hospitalName;
    @Column(name = "municipio")
    private String municipality;
    @Column(name = "provincia")
    private String province;
    @Column(name = "ccaa")
    private String ccaa;
    @Column(name = "pais")
    private String country;

    public hospital() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(long hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCcaa() {
        return ccaa;
    }

    public void setCcaa(String ccaa) {
        this.ccaa = ccaa;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "hospital{" +
                "id=" + id +
                ", hospitalCode=" + hospitalCode +
                ", hospitalName='" + hospitalName + '\'' +
                ", municipality='" + municipality + '\'' +
                ", province='" + province + '\'' +
                ", ccaa='" + ccaa + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
